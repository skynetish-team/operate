```python
                                                                       
   _|_|                                              _|                
 _|    _|  _|_|_|      _|_|    _|  _|_|    _|_|_|  _|_|_|_|    _|_|    
 _|    _|  _|    _|  _|_|_|_|  _|_|      _|    _|    _|      _|_|_|_|  
 _|    _|  _|    _|  _|        _|        _|    _|    _|      _|        
   _|_|    _|_|_|      _|_|_|  _|          _|_|_|      _|_|    _|_|_|  
           _|                                                          
           _|                                                          
```
Web interface for configuration of Redeem, Toggle, Cura and Octoprint

Installation on Debian:
```
sudo apt-get install control
```


Installation on Angstrom:  
For Angstrom, there is no new package. This project was called "thing-frontend" previously. Use that. 
```
opkg install thing-frontend
```

Installation from source:  
Caveat: Standard www-root has changed for lighttpd. It's either /www/pages or /www/pages/html on later distros. 
```
cd /usr/src
git clone https://intelligentagent@bitbucket.org/intelligentagent/operate.git
cd operate
mkdir -p /www/pages/
cp -r operate /var/www
```
```
nano /etc/init.d/led_aging.sh
```
Replace with this:
```
#!/bin/sh -e
### BEGIN INIT INFO
# Provides:          led_aging.sh
# Required-Start:    $local_fs
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start LED aging
# Description:       Starts LED aging (whatever that is)
### END INIT INFO

x=$(/bin/ps -ef | /bin/grep "[l]ed_acc")
if [ ! -n "$x" -a -x /usr/bin/led_acc ]; then
    /usr/bin/led_acc &
fi
```

```
apt-get install libsqlite3-0 
apt-get install lighttpd
```
Disable Apache etc. 
```
systemctl mask apache2.service
systemctl mask bonescript.service
systemclt mask cloud9.service
```
Install Flup
```
cd /usr/src
wget --no-check-certificate https://pypi.python.org/packages/source/f/flup/flup-1.0.tar.gz#md5=530801fe835fd9a680457e443eb95578
tar -vxf flup-1.0.tar.gz
cd flup-1.0/
python setup.py install
```
Configure fastCGI. Add this to 
/etc/lighttpd/conf-available/10-fastcgi.conf
```
 fastcgi.server = ( ".py" =>
(( "socket" => "/tmp/fastcgi.socket",
"bin-path" => "/www/pages/operate/operate.py",
"max-procs" => 1,
"bin-environment" => (
"REAL_SCRIPT_NAME" => ""
),
"check-local" => "disable"
))
)

url.rewrite-once = (
"^/favicon.ico$" => "/static/favicon.ico",
"^/static/(.*)$" => "/static/$1",
"^/(.*)$" => "/operate/operate.py/$1",
)

```
Enable fastcgi:
```
cd /etc/lighttpd/conf-enabled/
ln -s ../conf-available/10-fastcgi.conf 
```
