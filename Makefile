# $Id: Makefile, v 1.6 2015/3/29 croadfeldt Exp $
#

PYTHON=`which python`
DESTDIR=/
BUILDIR=$(CURDIR)/debian/operate
PROJECT=operate
VERSION=0.1.1

all:
	@echo "make source - Create source package"
	@echo "make install - Install on local system"
	@echo "make buildrpm - Generate a rpm package"
	@echo "make builddeb - Generate a deb package"
	@echo "make clean - Get rid of scratch and byte files"

source:
	echo "NOT USED"
	
install:
	echo "NOT USED"
	
buildrpm:
	echo "NOT USED"
	
builddeb:
	# build the source package in the parent directory
	# then rename it to project_version.orig.tar.gz
	mkdir -p $(BUILDIR)
	mkdir -p $(BUILDIR)/var/www/html
	mkdir -p $(BUILDIR)/etc/lighttpd/conf-available
	cp -Rp DEBIAN $(BUILDIR)
	cp -Rp scripts/20-operate.conf $(BUILDIR)/etc/lighttpd/conf-available
	cp -Rp operate $(BUILDIR)/var/www/html
	cp -Rp static $(BUILDIR)/var/www/html
	dpkg-deb --build $(BUILDIR)
	mv debian/operate.deb ../
	dpkg-name -o ../operate.deb
	
clean:
	rm -rf $(BUILDIR)
